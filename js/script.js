//jQuery Code to make dynamic changes for the Dropdown list
$(document).ready(function(){
    $("select").change(function(){
        $(this).find("option:selected").each(function() {
            var optionValue = $(this).attr("value");
            if(optionValue) {
                $(".box").not("." + optionValue).hide();
                $("." + optionValue).show();
            } else {
                $(".box").hide();
            }
        });
    }).change();
});

//jQuery code to check whether form fields are empty or not via Validation Plugin
$(document).ready(function() {
    $("#addForm").validate({
        errorClass: "error fail-alert",

        rules: {
            sku: "required",
            name: "required",
            price: "required",
            type: "required",
            dvdSize: "required",
            bookWeight: "required",
            furnitureHeight: "required",
            furnitureWidth: "required",
            furnitureLength: "required"
        },

        messages: {
            sku: "*PLEASE SUBMIT REQUIRED DATA!",
            name: "*PLEASE SUBMIT REQUIRED DATA!",
            price: "*PLEASE SUBMIT REQUIRED DATA!",
            dvdSize: "*PLEASE SUBMIT REQUIRED DATA!",
            bookWeight: "*PLEASE SUBMIT REQUIRED DATA!",
            furnitureHeight: "*PLEASE SUBMIT REQUIRED DATA!",
            furnitureWidth: "*PLEASE SUBMIT REQUIRED DATA!",
            furnitureLength: "*PLEASE SUBMIT REQUIRED DATA!",

            type: "*PLEASE SELECT DATA TYPE!" 
        }
    });
});

//Select/Unselect All Checkboxes
$(document).ready(function() {
    $("#select-all").on("click", function() {
        if(this.checked) {
            $(".checkbox").each(function() {
                this.checked = true;
            });
        } else {
            $(".checkbox").each(function() {
                this.checked = false;
            })
        };
    });

    $(".checkbox").on("click", function() {
        if($(".checkbox:checked").length == $(".checkbox").length) {
            $("#select-all").prop("checked", true);
        } else {
            $("#select-all").prop("checked", false);
        }
    });
});