<?php

//Child Class
class DVD extends Product {
    public $type = "DVD";

    public function setSKU() {
        $this->sku = htmlspecialchars(strip_tags($this->sku));
    }

    public function setName() {
        $this->name = htmlspecialchars(strip_tags($this->name));
    }

    public function setPrice() {
        $this->price = htmlspecialchars(strip_tags($this->price));
    }

    public function setType() {
        $this->type = htmlspecialchars(strip_tags($this->type));
    }

    public function setAttribute() {
        $this->attribute = $dvdSize;
        $this->dvdSize = htmlspecialchars(strip_tags($this->dvdSize));
    }


    public function create() {
        $query = "INSERT INTO 
                    " . $this->table_name . "(sku, name, price, type, dvdSize)
                VALUES
                    (?, ?, ?, ?, ?)";
        $stmt = $this->conn->prepare($query);

        $stmt->bindParam(1, $this->sku);
        $stmt->bindParam(2, $this->name);
        $stmt->bindParam(3, $this->price);
        $stmt->bindParam(4, $this->type);
        $stmt->bindParam(5, $this->dvdSize);

        if($stmt->execute()) {
            return true;
        } else {
            return false;
        }
    }

}
