<?php

//Child Class
class Furniture extends Product{
    public $type = "Furniture";

    public function setSKU() {
        $this->sku = htmlspecialchars(strip_tags($this->sku));
    }

    public function setName() {
        $this->name = htmlspecialchars(strip_tags($this->name));
    }

    public function setPrice() {
        $this->price = htmlspecialchars(strip_tags($this->price));
    }
        
    public function setType() {
        $this->type = htmlspecialchars(strip_tags($this->type));
    }
        
    public function setAttribute() {
        $this->attribute = $furnitureHeight;
        $this->attribute = $furnitureWidth;
        $this->attribute = $furnitureLength;
        $this->furnitureHeight = htmlspecialchars(strip_tags($this->furnitureHeight));
        $this->furnitureWidth = htmlspecialchars(strip_tags($this->furnitureWidth));
        $this->furnitureLength = htmlspecialchars(strip_tags($this->furnitureLength));
    }

    public function create() {
        $query = "INSERT INTO 
                    " . $this->table_name . "(sku, name, price, type, furnitureHeight, furnitureWidth, furnitureLength)
                VALUES
                    (?, ?, ?, ?, ?, ?, ?)";
        $stmt = $this->conn->prepare($query);

        $stmt->bindParam(1, $this->sku);
        $stmt->bindParam(2, $this->name);
        $stmt->bindParam(3, $this->price);
        $stmt->bindParam(4, $this->type);
        $stmt->bindParam(5, $this->furnitureHeight);
        $stmt->bindParam(6, $this->furnitureWidth);
        $stmt->bindParam(7, $this->furnitureLength);

        if($stmt->execute()) {
            return true;
        } else {
            return false;
        }
    }
}
