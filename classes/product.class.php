<?php

//Parent Class
class Product {

    //Database connection and table name
    public $conn;
    protected $table_name = "products";

    //Object properties
    public $sku; 
    public $name; 
    public $price; 
    public $type; 

    public function __construct($db) {
        $this->conn = $db;
    }
    
    //Create product
    public function create() {

    $query = "INSERT INTO 
                " . $this->table_name . "
                SET
                sku=:sku, name=:name, price=:price, type=:type";

    $stmt = $this->conn->prepare($query);
    // //Posted Values
    $this->sku = htmlspecialchars(strip_tags($this->sku));
    $this->name = htmlspecialchars(strip_tags($this->name));
    $this->price = htmlspecialchars(strip_tags($this->price));
    $this->type = htmlspecialchars(strip_tags($this->type));


    // //Bind Values
    $stmt->bindParam(":sku", $this->sku);
    $stmt->bindParam(":name", $this->name);
    $stmt->bindParam(":price", $this->price);
    $stmt->bindParam(":type", $this->type);
    


    if($stmt->execute()) {
        return true;
    } else {
        return false;
    }
    }
    
    //Delete product
    public function delete($id) {
    $query = "DELETE FROM
                " . $this->table_name . "
            WHERE
                id = :id";
    $stmt = $this->conn->prepare($query);
    $stmt->bindParam(":id", $id);

    $stmt->execute();

    return $stmt;
    }

    function getAllProducts() {

    //Write query
    $query = "SELECT
                *
                FROM
                " .$this->table_name . "
                ORDER BY
                id DESC";
    $stmt = $this->conn->prepare($query);
    $stmt->execute();

    return $stmt;
    }
}
?>