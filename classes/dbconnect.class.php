<?php

    //PDO
    class DBConnect {

        //Specify database credentials
        private $servername = "localhost";
        private $username = "root";
        private $password = "";
        private $dbname = "phpadddelete";

        //Connect to the database
        public function connect() {

            $this->conn = null;

            try {
                $this->conn = new PDO("mysql:host=" . $this->servername . ";dbname=" . $this->dbname, $this->username, $this->password);
            } catch(PDOException $exception) {
                echo "Connection Error: " . $exception->getMessage();
            }
            return $this->conn;
        }

    }
?>