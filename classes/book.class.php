<?php

//Child Class
class Book extends Product{
    public $type = "Book";

    public function setSKU() {
        $this->sku = htmlspecialchars(strip_tags($this->sku));
    }

    public function setName() {
        $this->name = htmlspecialchars(strip_tags($this->name));
    }

    public function setPrice() {
        $this->price = htmlspecialchars(strip_tags($this->price));
    }

    public function setType() {
        $this->type = htmlspecialchars(strip_tags($this->type));
    }

    public function setAttribute() {
        $this->attribute = $bookWeight;
        $this->bookWeight = htmlspecialchars(strip_tags($this->bookWeight));
    }


    public function create() {
        $query = "INSERT INTO 
                    " . $this->table_name . "(sku, name, price, type, bookWeight)
                VALUES
                    (?, ?, ?, ?, ?)";
        $stmt = $this->conn->prepare($query);

        $stmt->bindParam(1, $this->sku);
        $stmt->bindParam(2, $this->name);
        $stmt->bindParam(3, $this->price);
        $stmt->bindParam(4, $this->type);
        $stmt->bindParam(5, $this->bookWeight);

        if($stmt->execute()) {
            return true;
        } else {
            return false;
        }
    }

}
