<?php
include "includes/class-autoload.inc.php";

//NOTE: If the Class Autoloader above does not work (Happens in free web hosts like 000webhost or InfinityFree), then please uncomment the following Classes below.

// //Classes
// include "classes/dbconnect.class.php";
// include "classes/product.class.php";
// include "classes/dvd.class.php";
// include "classes/book.class.php";
// include "classes/furniture.class.php";

//Get database (DBConnect) connection
$database = new DBConnect();
$db = $database->connect();

//Pass connection to objects
$product = new Product($db);

//Query and get all Products
$stmt = $product->getAllProducts();
$products = $stmt->fetchAll(PDO::FETCH_ASSOC);

//Mass deletion of Products
if(isset($_POST["deleteButton"])) {
    // var_dump($_POST);
    // exit;
    foreach ($_POST["checkedProducts"] as $key => $id) {
        $product->delete($id);
    }
    header("Location: index.php");
    exit;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP Exercise: Product List</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/stylesheet.css">
</head>

<body>
<form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']);?>" method = "post">
    <header>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <h1>Product List</h1>
                <ul class="navbar-right">
                    
                    <li><a href = "add.php"><button type = "button">ADD</button></a></li>
                    <li><a href = ""><button type = "submit" name = "deleteButton">MASS DELETE</button></a></li>

                </ul>
            </div>
        </nav>
    </header>

    <div class = "container">
        <div class = "container-fluid-checkbox">
            <input type="checkbox" id = "select-all">
                <a>SELECT/UNSELECT ALL</a> 
            </input>
        </div>

        <div class = "row">
            <?php foreach($products as $product): ?>
                <div class = "col-sm-4 mb-5"> 
                    <div class = "card w-75">
                        <span class = "border border-dark">
                            <div class="container mb-5">
                                    <input type="checkbox" class="checkbox" name = "checkedProducts[]" value = "<?php echo $product["id"]?>">
                                        <div class = "card-content text-center ">
                                            <?php 
                                                echo htmlspecialchars($product["sku"]) . "<br>"; 
                                                echo htmlspecialchars($product["name"]). "<br>"; 
                                                echo htmlspecialchars($product["price"]). "$" . "<br>";

                                                if($product["type"] == "DVD") { 
                                                    echo "Size: " . htmlspecialchars($product["dvdSize"]) . " MB" . "<br>";
                                                } 

                                                if($product["type"] == "Book") { 
                                                    echo "Weight: " . htmlspecialchars($product["bookWeight"]) . " KG" . "<br>"; 
                                                } 

                                                if($product["type"] == "Furniture") { 
                                                    echo "Dimension " . htmlspecialchars($product["furnitureHeight"]) . "x" . htmlspecialchars($product["furnitureWidth"]) . "x" . htmlspecialchars($product["furnitureLength"]) . "<br>"; 
                                                }   
                                            ?>
                                        </div>
                                    </input>

                            </div>
                        </span>
                    </div>
                </div>
            <?php endforeach ?>
        </div>
    </div>

    <?php include("templates/footer.php"); ?>
    </form>

    <!--jQuery-->
    <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>

    <!--Link to the Javascript Code for the Select All Checkboxes function. -->
    <script type = "text/javascript" src="js/script.js"></script>
</body>                                             
</html>