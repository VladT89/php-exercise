<?php
include "includes/class-autoload.inc.php";

//NOTE: If the Class Autoloader above does not work (Happens in free web hosts like 000webhost or InfinityFree), then please uncomment the following Classes below.

// //Classes
// include "classes/dbconnect.class.php";
// include "classes/product.class.php";
// include "classes/dvd.class.php";
// include "classes/book.class.php";
// include "classes/furniture.class.php";

//Get database (DBConnect) connection
$database = new DBConnect();
$db = $database->connect();

//Pass connection to objects
//$product = new Product($db);
$dvd = new DVD($db);
$book = new Book($db);
$furniture = new Furniture($db);

//Code to submit product data by type and redirect to index.php
if(isset($_POST["submitButton"])) {

    if($_POST['type'] == 'DVD') {
        $dvd->dvdSize = $_POST['dvdSize'];

        $dvd->sku = $_POST['sku'];
        $dvd->name = $_POST['name'];
        $dvd->price = $_POST['price'];
        $dvd->type = $_POST['type'];
        $dvd->create();
    }
    else if($_POST['type'] == 'Book') {
        $book->bookWeight = $_POST['bookWeight'];

        $book->sku = $_POST['sku'];
        $book->name = $_POST['name'];
        $book->price = $_POST['price'];
        $book->type = $_POST['type'];
        $book->create();
    }
    else if($_POST['type'] == 'Furniture') {
        $furniture->furnitureHeight = $_POST['furnitureHeight'];
        $furniture->furnitureWidth = $_POST['furnitureWidth'];
        $furniture->furnitureLength = $_POST['furnitureLength'];

        $furniture->sku = $_POST['sku'];
        $furniture->name = $_POST['name'];
        $furniture->price = $_POST['price'];
        $furniture->type = $_POST['type'];
        $furniture->create();
    }
    header("Location: index.php");
    exit;
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP Exercise: Product Add</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" 
    integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/stylesheet.css">
</head>

<body>
    <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']);?>" id = "addForm" method = "post">
        <header>
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <h1>Product Add</h1>
                        <ul class="navbar-right">
                            <li><a href = ""><button type = "submit" name = "submitButton">Save</button></a></li>
                            <li><a href = "index.php"><button type = "button">Cancel</button></a></li>
                        </ul>
                </div>
            </nav>
        </header>

        <div class="container-fluid">

            <div class="product-add">
        
                <div class="form-group">
                    <label for="sku">SKU</label>
                    <input type="text" name="sku" id = "form_sku">
                </div>

                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" name="name">
                </div>

                <div class="form-group">
                    <label for="price">Price ($)</label>
                    <input type="number" name="price" step="0.01" >
                </div>

                <div class="form-group">
                    <label for="type">Product Type</label>
                        <select class="form-group" name="type">
                            <option selected="selected" disabled>Please click to choose</option>
                            <option value="DVD">DVD</option>
                            <option value="Book">Book</option>
                            <option value="Furniture">Furniture</option>
                        </select>
                </div>


                <div class = "form-group">
                    <div class = "DVD box">
                        <label for="DVD">Size (MB)</label>
                        <input type="number" name="dvdSize" step="0.01">
                        <p>Please provide storage size in MB format.</p>
                    </div>
                </div>
                

                <div class = "form-group">
                    <div class = "Book box">
                        <label for="Book">Weight (KG)</label>
                        <input type="number" name="bookWeight" step="0.01">
                        <p>Please provide weight in KG.</p>
                    </div>
                </div>


                <div class = "form-group">
                    <div class = "Furniture box">
                        <ul style="list-style-type:none" name="furniture">
                            <li>
                                <label for="furnitureHeight">Height (CM)</label>
                                <input type="number" name="furnitureHeight" step="0.01">
                            </li>

                            <li>
                                <label for="furnitureWidth">Width (CM)</label>
                                <input type="number" name="furnitureWidth" step="0.01">
                            </li>

                            <li>
                                <label for="furnitureLength">Length (CM)</label>
                                <input type="number" name="furnitureLength" step="0.01"> 
                            </li>
                        </ul>
                        <p>Please provide dimensions in HxWxL format.</p>
                    </div>
                </div>

            </div>

        </div>

    </form>

    <?php include("templates/footer.php"); ?>

    <!--jQuery-->
    <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>

    <!-- jQuery Validation CDN -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js" integrity="sha512-UdIMMlVx0HEynClOIFSyOrPggomfhBKJE28LKl8yR3ghkgugPnG6iLfRfHwushZl1MOPSY6TsuBDGPK2X4zYKg==" crossorigin="anonymous"></script>

    <!--Link to the Javascript Code for the Dynamic Change for the Dropdown list and for checking if form fields are emtpy or not. -->
    <script type = "text/javascript" src="js/script.js"></script>

</body>
</html>